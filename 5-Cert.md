<center>
<table>
<tbody>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999">-</td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">sudo openssl req -x509 -nodes -days 365 -new -newkey rsa:4096<br/>-out cert.pem -keyout privkey.pem<br/>-subj "/C=ID/ST=Indonesia/L=Jakarta/O=Base Company/OU=Information Communication Technology (ICT)/emailAddress=base.company@email.com/CN=base.com"</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">sudo certbot certonly --manual --preferred-challenges=dns --server https://acme-v02.api.letsencrypt.org/directory --agree-tos<br/>--email "base.company@email.com"<br/>--domain "base.com"</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>

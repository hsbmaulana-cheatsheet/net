<center>
<table>
<tbody>

<tr>
<td align="center" width="9999"><b><a href="https://medium.com/tekaje-id/virtualbox-mengenal-macam-dan-fungsi-network-modes-di-virtualbox-30bb02992e0a#9314">Virtualbox</a></b></td>
<td align="center" width="9999"><b>Vagrant</b></td>
<td align="center" width="9999"><b>Docker</b></td>
</tr>

<tr>
<td align="left" width="9999">Not Attached</td>
<td align="center" width="9999">None</td>
<td align="right" width="9999">None</td>
</tr>

<tr>
<td align="left" width="9999"><i>NAT</i></td>
<td align="center" width="9999"><i>Forwardport</i></td>
<td align="right" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="left" width="9999">NAT Network Service</td>
<td align="center" width="9999">&nbsp;</td>
<td align="right" width="9999"><i>Bridge</i></td>
</tr>

<tr>
<td align="left" width="9999">Bridged</td>
<td align="center" width="9999">Public</td>
<td align="right" width="9999">Host</td>
</tr>

<tr>
<td align="left" width="9999">Internal Network</td>
<td align="center" width="9999">&nbsp;</td>
<td align="right" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="left" width="9999">Host-Only</td>
<td align="center" width="9999">Private</td>
<td align="right" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="left" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
<td align="right" width="9999">Overlay</td>
</tr>

</tbody>
</table>
</center>

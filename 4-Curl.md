<center>
<table>
<tbody>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Execute Shell</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">curl -sL protocol://... | sh -s arg1 arg2 arg3</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Download</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">curl -OL protocol://...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Upload</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">curl -iL<br/>-H "Content-Type: multipart/form-data"<br/>-F "datafiles[]=@/home/folder/file.txt"<br/>protocol://...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>REST</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">curl -iL<br/>-H "Accept-Charset: utf-8"<br/>-H "Accept-Language: en-US|id-ID"<br/>-H "Accept: application/xml|json"<br/>-X DELETE|GET|OPTIONS<br/>protocol://...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">curl -iL<br/>-H "Accept-Charset: utf-8"<br/>-H "Accept-Language: en-US|id-ID"<br/>-H "Accept: application/xml|json"<br/>-H "Content-Type: application/x-www-form-urlencoded"<br/>-X POST|PUT|PATCH<br/>-d "x=value&y=value"<br/>protocol://...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">curl -iL<br/>-H "Accept-Charset: utf-8"<br/>-H "Accept-Language: en-US|id-ID"<br/>-H "Accept: application/xml|json"<br/>-H "Content-Type: multipart/form-data"<br/>-X POST|PUT|PATCH<br/>-F "x=value"<br/>-F "y=value"<br/>protocol://...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>AUTH</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">-H "Authorization: Basic ..."</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">-H "Authorization: Bearer ..."</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">-H "Authorization: OAuth ..."</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">--cookie "/home/folder/cookie.txt"<br/>--cookie-jar "/home/folder/cookie.txt"</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>

<center>
<table>
<tbody>

<tr>
<td align="center" width="9999"><b>IPv4</b></td>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>IPv6</b></td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ip address show</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">ip -4 address</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ip -6 address</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">ip link set ... up</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ip link set ... down</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Port</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">lsof<br/>-i <i>TCP|UDP</i>:port<br/>-s <i>TCP|UDP</i>:<i><br/>LISTEN|<br/>ESTABLISHED|<br/>CLOSING</i></td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">lsof +D /var/www/xyz/</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>

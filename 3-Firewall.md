<center>
<table>
<tbody>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>In & Out</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">/etc/services</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">ufw status numbered</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ufw status verbose</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ufw insert ...<br/>allow|deny|reject|limit<br/>in|out<br/>on ...<br/>from .../...<br/>to .../...<br/>.../tcp|udp</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ufw delete ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ufw reload</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Monitor Traffic</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">rsyslog</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ufw logging off</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ufw insert ...<br/>log|log-all<br/>in|out<br/>on ...<br/>from .../...<br/>to .../...<br/>.../tcp|udp</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ufw reload</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>
